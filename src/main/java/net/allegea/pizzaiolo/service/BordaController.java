package net.allegea.pizzaiolo.service;

import java.util.List;

import net.allegea.pizzaiolo.model.Borda;
import net.allegea.pizzaiolo.model.PizzariaService;
import net.allegea.pizzaiolo.support.BordaResource;
import net.allegea.pizzaiolo.support.BordaResourceAssembler;
import net.allegea.pizzaiolo.support.MessageResource;
import net.allegea.pizzaiolo.support.MessageResourceAssembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ExposesResourceFor(Borda.class)
@EnableHypermediaSupport
@RequestMapping("/borda")
public class BordaController {

	public interface Request {
		
		String ID = "/{id}";
		
	}
	
	@Autowired
	private BordaResourceAssembler resourceAssembler;

	@Autowired
	private MessageResourceAssembler messageResourceAssembler;
	
	@Autowired
	private PizzariaService service;
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<BordaResource> adicionar(@RequestHeader(value = "Version", required=false) String version, @RequestBody Borda body) {
		if(version.equals(Borda.CURRENT_VERSION)) {
			Borda borda = service.adicionarBorda(body);
			BordaResource resource = resourceAssembler.toResource(borda);
			
			return new ResponseEntity<BordaResource>(resource, HttpStatus.CREATED);
		}
		else {
			return new ResponseEntity<BordaResource>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
		}
	}

	@RequestMapping(method = RequestMethod.PUT, value = Request.ID)
	public ResponseEntity<BordaResource> atualizar(@RequestHeader(value = "Version", required=false) String version, @PathVariable("id") int id, @RequestBody Borda body) {
		Borda borda = service.atualizarBorda(id, body);
		BordaResource resource = resourceAssembler.toResource(borda);
		
		return new ResponseEntity<BordaResource>(resource, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<BordaResource>> bordas(@RequestHeader(value = "Version", required=false) String version) {
		List<Borda> bordas = service.obterBordas();
		List<BordaResource> resource = resourceAssembler.toResources(bordas);
		
		return new ResponseEntity<List<BordaResource>>(resource, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = Request.ID)
	public ResponseEntity<BordaResource> borda(@RequestHeader(value = "Version", required=false) String version, @PathVariable("id") int id) {
		Borda borda = service.obterBorda(id);
		BordaResource resource = resourceAssembler.toResource(borda);
		
		return new ResponseEntity<BordaResource>(resource, HttpStatus.OK);
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<MessageResource> handleExceptions(Exception ex) {
		MessageResource resource = messageResourceAssembler.toResource(2, ex.getMessage());
		
		return new ResponseEntity<MessageResource>(resource, HttpStatus.BAD_REQUEST);
	}
	
}
