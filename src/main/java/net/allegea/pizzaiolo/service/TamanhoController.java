package net.allegea.pizzaiolo.service;

import java.util.List;

import net.allegea.pizzaiolo.model.PizzariaService;
import net.allegea.pizzaiolo.model.Tamanho;
import net.allegea.pizzaiolo.support.MessageResource;
import net.allegea.pizzaiolo.support.MessageResourceAssembler;
import net.allegea.pizzaiolo.support.TamanhoResource;
import net.allegea.pizzaiolo.support.TamanhoResourceAssembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ExposesResourceFor(Tamanho.class)
@EnableHypermediaSupport
@RequestMapping("/tamanho")
public class TamanhoController {

	public interface Request {
		
		String ID = "/{id}";
		
	}

	@Autowired
	private TamanhoResourceAssembler resourceAssembler;

	@Autowired
	private MessageResourceAssembler messageResourceAssembler;
	
	@Autowired
	private PizzariaService service;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<TamanhoResource> adicionar(@RequestHeader(value = "Version", required=false) String version, @RequestBody Tamanho body) {
		Tamanho tamanho = service.adicionarTamanho(body);
		TamanhoResource resource = resourceAssembler.toResource(tamanho);
		
		return new ResponseEntity<TamanhoResource>(resource, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.PUT, value = Request.ID)
	public ResponseEntity<TamanhoResource> atualizar(@RequestHeader(value = "Version", required=false) String version, @PathVariable("id") int id, @RequestBody Tamanho body) {
		Tamanho tamanho = service.atualizarTamanho(id, body);
		TamanhoResource resource = resourceAssembler.toResource(tamanho);
		
		return new ResponseEntity<TamanhoResource>(resource, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<TamanhoResource>> tamanhos(@RequestHeader(value = "Version", required=false) String version) {
		List<Tamanho> tamanhos = service.obterTamanhos();
		List<TamanhoResource> resource = resourceAssembler.toResources(tamanhos);
		
		return new ResponseEntity<List<TamanhoResource>>(resource, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = Request.ID)
	public ResponseEntity<TamanhoResource> tamanho(@RequestHeader(value = "Version", required=false) String version, @PathVariable("id") int id) {
		Tamanho tamanho = service.obterTamanho(id);
		TamanhoResource resource = resourceAssembler.toResource(tamanho);
		
		return new ResponseEntity<TamanhoResource>(resource, HttpStatus.OK);
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<MessageResource> handleExceptions(Exception ex) {
		MessageResource resource = messageResourceAssembler.toResource(2, ex.getMessage());
		
		return new ResponseEntity<MessageResource>(resource, HttpStatus.BAD_REQUEST);
	}
	
}
