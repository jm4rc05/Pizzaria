package net.allegea.pizzaiolo.model;

import org.springframework.hateoas.Identifiable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Borda implements Identifiable<Integer> {
	
	public static String CURRENT_VERSION = "1";
	
	private int id;

	private String nome;
	
	private double valor;

	public Borda() {
		super();
		
		this.nome = "";
		this.valor = 0;
	}
	
	public Borda(String nome, double valor) {
		super();
		
		this.nome = nome;
		this.valor = valor;
	}

	@JsonIgnore
	public Integer getId() {
		return id;
	}

	@JsonIgnore
	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}
	
}
