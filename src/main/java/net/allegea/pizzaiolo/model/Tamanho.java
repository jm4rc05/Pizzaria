package net.allegea.pizzaiolo.model;

import org.springframework.hateoas.Identifiable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Tamanho implements Identifiable<Integer> {
	
	public static String CURRENT_VERSION = "1";
	
	private int id;

	private String nome;
	
	private int pedacos;

	public Tamanho() {
		super();
		
		this.nome = "";
		this.pedacos = 0;
	}
	
	public Tamanho(String nome, int pedacos) {
		super();
		
		this.nome = nome;
		this.pedacos = pedacos;
	}

	@JsonIgnore
	public Integer getId() {
		return id;
	}

	@JsonIgnore
	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getPedacos() {
		return pedacos;
	}

	public void setPedacos(int pedacos) {
		this.pedacos = pedacos;
	}
	
}
