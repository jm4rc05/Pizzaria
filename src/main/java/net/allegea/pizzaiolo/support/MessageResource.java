package net.allegea.pizzaiolo.support;

import net.allegea.pizzaiolo.model.Message;

import org.springframework.hateoas.ResourceSupport;

public class MessageResource extends ResourceSupport {
	
	private Message message;

	public MessageResource(Message message) {
		super();
		
		this.message = message;
	}

	public Message getMessage() {
		return message;
	}

}
