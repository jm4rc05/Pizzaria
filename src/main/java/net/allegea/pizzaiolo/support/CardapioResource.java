package net.allegea.pizzaiolo.support;

import net.allegea.pizzaiolo.model.Cardapio;

import org.springframework.hateoas.ResourceSupport;

public class CardapioResource extends ResourceSupport {
	
	private Cardapio cardapio;

	public CardapioResource(Cardapio cardapio) {
		super();
		
		this.cardapio = cardapio;
	}

	public Cardapio getCardapio() {
		return cardapio;
	}

}
