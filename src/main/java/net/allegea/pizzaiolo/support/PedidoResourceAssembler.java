package net.allegea.pizzaiolo.support;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import net.allegea.pizzaiolo.model.Borda;
import net.allegea.pizzaiolo.model.Cobertura;
import net.allegea.pizzaiolo.model.Massa;
import net.allegea.pizzaiolo.model.Pedido;
import net.allegea.pizzaiolo.model.Tamanho;
import net.allegea.pizzaiolo.service.PedidoController;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class PedidoResourceAssembler extends ResourceAssemblerSupport<Pedido, PedidoResource> {

	public PedidoResourceAssembler() {
		super(PedidoController.class, PedidoResource.class);
	}

	public PedidoResource toResource(Pedido pedido) {
		PedidoResource resource = createResourceWithId(pedido.getId(), pedido);
		resource.add(linkTo(methodOn(PedidoController.class).massa(Massa.CURRENT_VERSION, pedido.getId())).withRel(LinkRelationName.PEDIDO_MASSA));
		resource.add(linkTo(methodOn(PedidoController.class).borda(Borda.CURRENT_VERSION, pedido.getId())).withRel(LinkRelationName.PEDIDO_BORDA));
		resource.add(linkTo(methodOn(PedidoController.class).coberturas(Cobertura.CURRENT_VERSION, pedido.getId())).withRel(LinkRelationName.PEDIDO_COBERTURA));
		resource.add(linkTo(methodOn(PedidoController.class).tamanho(Tamanho.CURRENT_VERSION, pedido.getId())).withRel(LinkRelationName.PEDIDO_TAMANHO));
		
		return resource;
	}
	
	@Override
	protected PedidoResource instantiateResource(Pedido pedido) {
		return new PedidoResource(pedido);
	}
	
}
