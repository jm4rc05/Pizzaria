package net.allegea.pizzaiolo.support;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import net.allegea.pizzaiolo.model.Cardapio;
import net.allegea.pizzaiolo.service.CardapioController;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class CardapioResourceAssembler extends ResourceAssemblerSupport<Cardapio, CardapioResource> {
	
	public static final String REL_CARDAPIO_COBERTURAS = "cardapio/coberturas";

	public CardapioResourceAssembler() {
		super(CardapioController.class, CardapioResource.class);
	}
	
	public CardapioResource toResource(Cardapio cardapio) {
		CardapioResource resource = createResourceWithId(cardapio.getId(), cardapio);
		resource.add(linkTo(methodOn(CardapioController.class).coberturas(Cardapio.CURRENT_VERSION, cardapio.getId())).withRel(REL_CARDAPIO_COBERTURAS));
		
		return resource;
	}

	@Override
	protected CardapioResource instantiateResource(Cardapio cardapio) {
		return new CardapioResource(cardapio);
	}
	
}
