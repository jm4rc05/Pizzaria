package net.allegea.pizzaiolo.support;

import net.allegea.pizzaiolo.model.Massa;

import org.springframework.hateoas.ResourceSupport;

public class MassaResource extends ResourceSupport {
	
	private Massa massa;
	
	public MassaResource(Massa massa) {
		super();
		
		this.massa = massa;
	}
	
	public Massa getMassa() {
		return massa;
	}

}
