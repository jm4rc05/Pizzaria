package net.allegea.pizzaiolo.support;

import net.allegea.pizzaiolo.model.Pedido;

import org.springframework.hateoas.ResourceSupport;

public class PedidoResource extends ResourceSupport {
	
	private Pedido pedido;
	
	public PedidoResource(Pedido pedido) {
		super();
		
		this.pedido = pedido;
	}
	
	public Pedido getPedido() {
		return pedido;
	}

}
