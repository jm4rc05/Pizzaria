package net.allegea.pizzaiolo.support;

import net.allegea.pizzaiolo.model.Tamanho;
import net.allegea.pizzaiolo.service.TamanhoController;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class TamanhoResourceAssembler extends ResourceAssemblerSupport<Tamanho, TamanhoResource> {

	public TamanhoResourceAssembler() {
		super(TamanhoController.class, TamanhoResource.class);
	}
	
	public TamanhoResource toResource(Tamanho tamanho) {
		TamanhoResource resource = createResourceWithId(tamanho.getId(), tamanho);
		
		return resource;
	}

	@Override
	protected TamanhoResource instantiateResource(Tamanho tamanho) {
		return new TamanhoResource(tamanho);
	}
	
}
