package net.allegea.pizzaiolo.support;

import net.allegea.pizzaiolo.model.Cobertura;

import org.springframework.hateoas.ResourceSupport;

public class CoberturaResource extends ResourceSupport {

	private Cobertura cobertura;

	public CoberturaResource(Cobertura cobertura) {
		super();
		
		this.cobertura = cobertura;
	}

	public Cobertura getCobertura() {
		return cobertura;
	}
	
}
