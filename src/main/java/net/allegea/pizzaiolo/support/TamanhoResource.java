package net.allegea.pizzaiolo.support;

import net.allegea.pizzaiolo.model.Tamanho;

import org.springframework.hateoas.ResourceSupport;

public class TamanhoResource extends ResourceSupport {
	
	private Tamanho tamanho;
	
	public TamanhoResource(Tamanho tamanho) {
		super();
		
		this.tamanho = tamanho;
	}
	
	public Tamanho getTamanho() {
		return tamanho;
	}

}
