package net.allegea.pizzaiolo.support;

import net.allegea.pizzaiolo.model.Borda;

import org.springframework.hateoas.ResourceSupport;

public class BordaResource extends ResourceSupport {
	
	private Borda borda;

	public BordaResource(Borda borda) {
		super();
		
		this.borda = borda;
	}

	public Borda getBorda() {
		return borda;
	}

}
